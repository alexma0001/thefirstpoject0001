## Heading h2

### Heading h3

#### Heading h4


Text A
<!-- blank line -->
<br>
<!-- blank line -->
Text B

Text
<!-- blank line -->
----
<!-- blank line -->
Text


This is **bold** and this is _italic_.


[Text to display][identifier] will display a link.

[Another text][another-identifier] will do the same. Hover the mouse over it to see the title.

[This link] will do the same as well. It works as the identifier itself.

[This link][] (same as above), has a second pair of empty brakets to indicate that the following parenthesis does not contain a link.

<https://example.com> works too. Must be used for explicit links.

<!-- Identifiers, in alphabetical order -->

[another-identifier]: https://example.com "This example has a title"
[identifier]: http://example1.com
[this link]: http://example2.com


1. Item one
   1. Sub item one
   2. Sub item two
   3. Sub item three
2. Item two


1.   Item 1
1.   Item 2

     ```ruby
     def hello
        puts "Hello world!"
     end
     ```

1.   Item 3
